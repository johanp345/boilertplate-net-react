import {
  startGetConfig,
  setGlobalConfig,
  errorGetConfig,
  setIdioma,
} from '.';
//import API from '../../services/Services';

export const getGlobalConfig = (domain) => async (dispatch) => {
  // try {
  //   dispatch(startGetConfig());
  //   const { status, data } = await API.config.getConfig(domain);
  //   if (status === 200) {
  //     dispatch(setGlobalConfig(data));
  //   } else {
  //     dispatch(errorGetConfig('Error in get Config.'));
  //   }
  // } catch (error) {
  //   dispatch(errorGetConfig('Error in get Config.'));
  // }
};

export const getIdioma = (idioma) => async (dispatch) => {
  try {
      dispatch(setIdioma(idioma));
  } catch (error) {
      dispatch(errorGetConfig('Error in get idioma.'));
  }
};
