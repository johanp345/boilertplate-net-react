import {
  GET_GLOBAL_CONFIG_START,
  GET_GLOBAL_CONFIG_SUCCESS,
  GET_GLOBAL_CONFIG_ERROR,
  SET_IDIOMA_VALUE,
} from "./types";

export const setGlobalConfig = (config) => ({
  type: GET_GLOBAL_CONFIG_SUCCESS,
  payload: config,
});

export const startGetConfig = () => ({
  type: GET_GLOBAL_CONFIG_START,
  payload: true,
});

export const errorGetConfig = (error) => ({
  type: GET_GLOBAL_CONFIG_ERROR,
  payload: error,
});


export const setIdioma = (value) => ({
  type: SET_IDIOMA_VALUE,
  payload: value,
});


