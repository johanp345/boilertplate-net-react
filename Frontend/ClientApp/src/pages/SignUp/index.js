import { connect } from "react-redux";
import SignUp from "./SignUp";

import { getGlobalConfig} from "../../actions/global/creators";

const mapStateToProps = (state) => ({
  global: state.global,
});

const mapDispatchToProps = (dispatch) => ({
  getGlobalConfig: (domain) => dispatch(getGlobalConfig(domain)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
