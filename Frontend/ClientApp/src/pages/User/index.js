import { connect } from "react-redux";
import User from "./User";

import { getGlobalConfig} from "../../actions/global/creators";

const mapStateToProps = (state) => ({
  global: state.global,
});

const mapDispatchToProps = (dispatch) => ({
  getGlobalConfig: (domain) => dispatch(getGlobalConfig(domain)),
});

export default connect(mapStateToProps, mapDispatchToProps)(User);
