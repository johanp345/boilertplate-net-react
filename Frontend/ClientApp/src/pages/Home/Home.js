import React, { useState,useEffect } from 'react';
import { withTranslation } from 'react-i18next';

//const cookies = new Cookies();
const Home = (props)=> {
  const {global,t} = props;
  useEffect(()=>{
    console.log(props);
  })
    return (
      <>
        <p>{t('tituloHome')}</p>
      </>
    );
}

export default withTranslation()(Home)