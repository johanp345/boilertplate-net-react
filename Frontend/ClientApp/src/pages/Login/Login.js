import React, { useState,useEffect } from 'react';
import { withTranslation } from 'react-i18next';
import {Link} from 'react-router-dom';
import logoCli from '../../assets/images/logo-cli.svg';

//const cookies = new Cookies();
const Login = (props)=> {
  const {global,t} = props;
  const [Form,setForm]=useState({
    usuario:"",
    password:""
  })
  useEffect(()=>{
    console.log(props);
  })
  const submit=(e)=>{
    e.preventDefault();
    console.log("submit");
    console.log(Form);
  }
  const handleForm=(e)=>{
    let {name,value}=e.target;
    setForm({
      ...Form,
      [name]:value
    })
  }
    return (
      <div className="d-flex align-items-stretch w-100 h-100" id="pageLogin">
        <section className="w-100 bg-ppal">

        </section>
        <section className="w-100 d-flex align-items-center justify-content-center">
          <section className="wrapLogin">
            <figure>
              <img src={logoCli}/>
            </figure>
            <h2 className="cTexto1 f20">{t('bienvenido')}</h2>
            <form onSubmit={submit} autoComplete="new-password">
              <div className="row">
                <div className="col-12">
                  <div className="wrapInput">
                    <input onChange={handleForm} type="text" value={Form.usuario} name="usuario" autoComplete="new-password"/>
                    <label className="placeHolder">{t('formUsuario')}</label>
                  </div>
                </div>
                <div className="col-12">
                  <div className="wrapInput">
                    <input onChange={handleForm} type="password" value={Form.password} name="password" autoComplete="new-password"/>
                    <label className="placeHolder">{t('formClave')}</label>
                  </div>
                </div>
                <div className="col-12">
                  <Link to='/forgotPass' className="float-right cTexto2">{t('olvideClave')}</Link>
                </div>
                <div className="col-12">
                  <button className="btn btn-block btnGeneral"><span>{t('buttonLogin')}</span></button>
                </div>
              </div>
            </form>
          </section>
        </section>
      </div>
    );
}

export default withTranslation()(Login)