import React, { Component } from 'react';

export const Layout = (props) => {
  const {children}= props 
  const layout = props.layout?props.layout:'completo';
    return (
      <>
          {(layout==='completo')?
            <div className='layoutCompleto w-100'>
                {children}
            </div>
            :
            <div className='layoutMenuLeft'>
                {children}
            </div>
          }
      </>
    );
}
