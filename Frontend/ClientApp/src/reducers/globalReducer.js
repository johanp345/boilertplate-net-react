import {
  GET_GLOBAL_CONFIG_SUCCESS,
  GET_GLOBAL_CONFIG_ERROR,
  GET_GLOBAL_CONFIG_START,
  SET_IDIOMA_VALUE,
} from '../actions/global/types';

const initialState = {
  isLoaded: 0,
  Idioma: 'es',
};

const globalReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_GLOBAL_CONFIG_START: {
      return {
        ...state,
        loading: action.payload,
      };
    }
    case GET_GLOBAL_CONFIG_SUCCESS: {
      return {
        ...state,
        ...action.payload,
        loading: false,
      };
    }
    case GET_GLOBAL_CONFIG_ERROR: {
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    }
    case SET_IDIOMA_VALUE: {
      return {
        ...state,
        Idioma: action.payload,
      };
    }
    default:
      return state;
  }
};

export default globalReducer;
