import React, { useState,useEffect } from 'react';
import { Switch, Route,Redirect } from 'react-router-dom';
import Cookies from 'universal-cookie';
import { Layout } from '../components/Layout';
import Login from '../pages/Login';
import SignUp from '../pages/SignUp';
import Home from '../pages/Home';
import User from '../pages/User';
import _404 from '../pages/404';
import "bootstrap";
import "../assets/css/main.scss";
import "../assets/css/theme.scss";
import "../assets/css/responsive.scss";

const cookies = new Cookies();
export default (props)=> {
  //const login = true || cookies.get('username');
  const login = cookies.get('username');

  useEffect(()=>{
    console.log(props);
  },[])
    return (
      <>
        {
          (!login)?
              <Layout>
                <Switch>
                    <Route  exact path='/login/' component={Login} />
                    <Route  exact path='/signup/' component={SignUp} />
                    <Route  path='' render ={()=><Redirect to="/login/"/>} />
                </Switch>
              </Layout>
            :
            <Layout layout="menu">
              <Switch>
                <Route exact path='/' component={Home} />
                <Route path='/user' component={User} />
                <Route  path='' component={_404} />
              </Switch>
            </Layout>
        }
      </>
    );
}
